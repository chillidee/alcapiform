<?php

/**
 * Plugin Name: ALC API Form
 * Plugin URI: http://www.australianlendingcentre.com.au
 * Description: ALC API Enquire Form for Money Maker
 * Version: 1.0
 * Author: Tim
 * Author URI: http://www.loong.com.au
 * License: ALC Copy Right, All Rights Reserved.
 */
function register_alcapiform_session()
{
    if (!session_id()) {
        session_start();
    }

    if (!empty($_SERVER['HTTP_REFERER'])) {
        //if the referer is website itself, then ignore. otherwise, go ahead.
        if (!TimStartsWith($_SERVER['HTTP_REFERER'], 'http://' . $_SERVER['SERVER_NAME']) &&
            !TimStartsWith($_SERVER['HTTP_REFERER'], 'https://' . $_SERVER['SERVER_NAME'])) {
            $_SESSION['mm_referalurl'] = $_SERVER['HTTP_REFERER'];
        }
    }

    if (isset($_REQUEST['t'])) {
        $_SESSION['mm_adwords_t'] = $_REQUEST['t'];
    }
    if (isset($_REQUEST['k'])) {
        $_SESSION['mm_adwords_k'] = $_REQUEST['k'];
    }
    if (isset($_REQUEST['a'])) {
        $_SESSION['mm_adwords_a'] = $_REQUEST['a'];
    }
    if (isset($_REQUEST['ref'])) {
        $_SESSION['mm_adwords_ref'] = $_REQUEST['ref'];
    }
    if (isset($_REQUEST['ne'])) {
        $_SESSION['mm_new_enquiry'] = $_REQUEST['ne'];
    }
}

add_action('init', 'register_alcapiform_session');

function alcapiform_shortcode_api_contact_form($atts, $content = null)
{

    //load jquery if not loaded
    if (!wp_script_is('jquery')) {
        wp_enqueue_script('jquery');
    }

    //detect platform
    $platform = 'desktop';
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        $platform = 'mobile';
    }

    //echo $useragent.'|'.$platform;

    $contact_form = "";

    $options = get_option('alcapiform_option_name');

    //load company ID - required
    $_cid = isset($atts['cid']) ? $atts['cid'] : $options['cid'];
    if (empty($_cid)) {
        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
                    <status>Not Ok</status>
                    <response_time>96.777</response_time>
                   </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Some Error Happend in your website:' . $_SERVER['SERVER_NAME'] . '';
        $body = 'Error: CID is not set.';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent = wp_mail($to, $subject, $body, $headers);
        if (!$sent):
            return 'Email Not sent';
        endif;

        return 'Error: CID is not set. ';
    }

    //load API GET URL - required
    $_geturl = isset($atts['geturl']) ? $atts['geturl'] : $options['geturl'];
    if (empty($_geturl)) {
        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
         <status>Not Ok</status>
         <response_time>96.777</response_time>
        </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Error: API GET URL is not set. ';
        $body = 'The Error happend in ' . $_SERVER['SERVER_NAME'] . '';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent = wp_mail($to, $subject, $body, $headers);
        if (!$sent):
            return 'Error: API GET URL is not set. ';
        endif;

        return 'Error: API GET URL is not set. ';
    }
    if (substr($_geturl, -1) != '/') {
        $_geturl .= '/';
    }

    //load API POST URL - required
    $_posturl = isset($atts['posturl']) ? $atts['posturl'] : $options['posturl'];
    if (empty($_posturl)) {
        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
         <status>Not Ok</status>
         <response_time>96.777</response_time>
        </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Error: API POST URL is not set. ';
        $body = 'The Error happend in ' . $_SERVER['SERVER_NAME'] . '';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent = wp_mail($to, $subject, $body, $headers);
        if (!$sent):
            return 'Error: API POST URL is not set. ';
        endif;

        return 'Error: API POST URL is not set. ';
    }

    $XML = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <pingdom_http_custom_check>
            <status>OK</status>
            <response_time>96.777</response_time>
        </pingdom_http_custom_check>';
    echo '<pre style="display:none;">' . $XML . '</pre>';

    //load enquiry form css file
    $_css = isset($atts['css']) ? $atts['css'] : $options['css'];
    if (!empty($_css)) {
        $contact_form .= '<link href="' . $_css . '" rel="stylesheet" type="text/css" />';
    }

    //load enquiry form js file
    $_js = isset($atts['js']) ? $atts['js'] : $options['js'];
    if (!empty($_js)) {
        $contact_form .= '<script src="' . $_js . '" type="text/javascript"></script>';
    }

    $_privacy = isset($atts['privacy']) ? $atts['privacy'] : $options['privacy'];
    if (!empty($_privacy)) {
        $_privacy = '/privacy-policy/';
    }

    $_rules_csv = isset($atts['rules_csv']) ? $atts['rules_csv'] : $options['rules_csv'];

    $_psurl = isset($atts['pssurl']) ? $atts['pssurl'] : $options['pssurl'];

    $_partner_email = isset($atts['partner_email']) ? $atts['partner_email'] : $options['partner_email'];

    $_from_email = isset($atts['from_email']) ? $atts['from_email'] : $options['from_email'];

    $company = isset($atts['company_name']) ? $atts['company_name'] : $options['company_name'];

    $_privacy_link_text = isset($atts['privacy_link_text']) ? $atts['privacy_link_text'] : $options['privacy_link_text'];

    $_privacy_text = isset($atts['privacy_text']) ? $atts['privacy_text'] : $options['privacy_text'];

    $_sendemails = isset($atts['send_emails']) ? $atts['send_emails'] : $options['send_emails'];

    $_send_sms_customer = isset($atts['send_sms_customer']) ? $atts['send_sms_customer'] : $options['send_sms_customer'];

    $_sms_account_id = isset($atts['sms_account_id']) ? $atts['sms_account_id'] : $options['sms_account_id'];

    $_sms_user_email = isset($atts['sms_user_email']) ? $atts['sms_user_email'] : $options['sms_user_email'];

    $_sms_user_password = isset($atts['sms_user_password']) ? $atts['sms_user_password'] : $options['sms_user_password'];

    $_sms_message = isset($atts['sms_message']) ? $atts['sms_message'] : $options['sms_message'];

    $_ask_preferred_time_call_back = isset($atts['ask_preferred_time_call_back']) ? $atts['ask_preferred_time_call_back'] : $options['ask_preferred_time_call_back'];

    //load jquery if specified
    $_jquery = isset($atts['jquery']) ? $atts['jquery'] : $options['jquery'];
    if (!empty($_jquery)) {
        $contact_form .= '<script src="' . $_jquery . '"></script>';
    }

    $contact_form .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>';

    //do form submission

    if (isset($_POST['alcapiformsumbit'])) {

        $questionsanswers = "";
        $answers = "";
        $rules_alcform_csv = "NO RULES";
        $rule_msg = "Not defined. You can add the rule on rules_alcform.csv as needed.";
        $loanAmount2 = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
        $comments2 .= 'Customer comments:<br> ' . $_POST['comments'] . ' <br> ';

        if ($_POST['hasProperty'] == '0' && ($_POST['typeOfLoan'] == '17' || $_POST['typeOfLoan'] == '2' || $_POST['typeOfLoan'] == '135') && $loanAmount2 > 2500 && $loanAmount2 < 50001) {

            $rules_alcform_csv = "RULES";

            $commetshasAuvisa = ($_POST['hasAuvisa'] == 1) ? "Yes" : "No";
            $comments2 .= '- Australian citizen, permanent resident or visa? - ' . $commetshasAuvisa . ' <br> ';

            $commetsHasbankrupt = ($_POST['bankrupt'] == 1) ? "Yes" : "No";
            $comments2 .= '- Bankrupt or Debt Agreement in past 10 years? - ' . $commetsHasbankrupt . ' <br> ';

            if ($_POST['bankrupt'] == '1') {
                $commetsdischarged = (isset($_POST['discharged']) && $_POST['discharged'] == 1) ? "Yes" : "No";
                $comments2 .= '- Have you been discharged? - ' . $commetsdischarged . ' <br> ';}

            $commetscurrentempl = ($_POST['currentempl'] == 1) ? "Yes" : "No";
            $comments2 .= '- Currently employed? - ' . $commetscurrentempl . ' <br> ';

            if ($_POST['currentempl'] == '1') {
                $commetscurrentjobtime = ($_POST['currentjob2yr'] == 1) ? "Yes" : "No";
                $comments2 .= '- Length at current job?(more than 1 year) - ' . $commetscurrentjobtime . ' <br> ';}

            $commetsrecentjudge = ($_POST['recentjudge'] == Yes) ? "Yes" : "No";
            $comments2 .= '- Any judgments or defaults? - ' . $commetsrecentjudge . ' <br> ';

            if ($_POST['recentjudge'] == 'Yes') {
                $commetsbepaid = ($_POST['bepaid'] == 1) ? "Yes" : "No";
                $comments2 .= '- Paid or unpaid? - ' . $commetsbepaid . ' <br> ';}

            if ($_POST['bepaid'] == '1') {
                $commetsjudgetime = ($_POST['judgetime'] == 1) ? "Yes" : "No";
                $comments2 .= '- When did you pay your default/judgement?(more than 4 years) - ' . $commetsjudgetime . ' <br> ';}

            $commetscurrentaddr = ($_POST['currentaddr'] == 1) ? "Yes" : "No";
            $comments2 .= '- Length at current address?(more than 1 year) - ' . $commetscurrentaddr . ' <br> ';

            $commetsaudrivelic = ($_POST['audrivelic'] == 1) ? "Yes" : "No";
            $comments2 .= '- Australian driver licence - ' . $commetsaudrivelic . ' <br> ';

            $commetsauunseureddebts = isUnsecuredDebtsOver8K() ? "Yes" : "No";
            $comments2 .= '- Unsecure debts of $8,000 or more? - ' . $commetsauunseureddebts . ' ';

            if ($_POST['bankrupt'] !== '0' || isUnsecuredDebtsOver8K() !== true) {

                if ($loanAmount2 > 5000) {$LenderID = 'PPL';}
                if ($loanAmount2 < 5000) {$LenderID = 'DEC';}

                //if ($_POST['audrivelic'] == '1' && ($_POST['bepaid'] == '0' || $_POST['discharged'] == '1' || $_POST['judgetime'] == '0' || $_POST['currentaddr'] == '0' || $_POST['currentjob2yr'] == '0') && $loanAmount2 <= 30000) {$LenderID = 'LNU';}
                if ($_POST['hasAuvisa'] == '0' || $_POST['discharged'] == '0' || $_POST['currentempl'] == '0') {$LenderID = 'DEC';}

                if ($_POST['audrivelic'] == '0' && ($_POST['bepaid'] == '0' || $_POST['discharged'] == '1' || $_POST['judgetime'] == '0' || $_POST['currentaddr'] == '0' || $_POST['currentjob2yr'] == '0')) {$LenderID = 'DEC';}
            }

            //Gather info from answers, then define lender

            //Citizen/Visa
            $questionsanswers .= "<br>Citizen/Visa: ";
            $questionsanswers .= ($_POST['hasAuvisa'] == 1) ? "Y" : "N";
            $answers .= ($_POST['hasAuvisa'] == 1) ? "Y" : "N";

            //DA/Bankrupt
            $questionsanswers .= "<br>DA/Bankrupt: ";
            $questionsanswers .= ($_POST['bankrupt'] == 1) ? "Y" : "N";
            $answers .= ($_POST['bankrupt'] == 1) ? "Y" : "N";

            //Discharged More than 10 Years
            $questionsanswers .= "<br>Discharged More than 10 Years: ";
            if ($_POST['bankrupt'] == 1) {
                $questionsanswers .= (isset($_POST['discharged']) && $_POST['discharged'] == 1) ? "Y" : "N";
                $answers .= (isset($_POST['discharged']) && $_POST['discharged'] == 1) ? "Y" : "N";
            } else {
                $questionsanswers .= "-";
                $answers .= "-";
            }

            //Employed
            $questionsanswers .= "<br>Employed: ";
            $questionsanswers .= ($_POST['currentempl'] == 1) ? "Y" : "N";
            $answers .= ($_POST['currentempl'] == 1) ? "Y" : "N";

            //More or Less than Year
            $questionsanswers .= "<br>More or Less than Year: ";
            if ($_POST['currentempl'] == 1) {
                $questionsanswers .= (isset($_POST['currentjob2yr']) && $_POST['currentjob2yr'] == 1) ? "Y" : "N";
                $answers .= (isset($_POST['currentjob2yr']) && $_POST['currentjob2yr'] == 1) ? "Y" : "N";
            } else {
                $questionsanswers .= "-";
                $answers .= "-";
            }

            //Judgement/Default
            $questionsanswers .= "<br>Judgement/Default: ";
            $questionsanswers .= ($_POST['recentjudge'] == Yes) ? "Y" : "N";
            $answers .= ($_POST['recentjudge'] == Yes) ? "Y" : "N";

            //Paid/Unpaid
            $questionsanswers .= "<br>Paid/Unpaid: ";
            if ($_POST['recentjudge'] == Yes) {
                $questionsanswers .= (isset($_POST['bepaid']) && $_POST['bepaid'] == 1) ? "Y" : "N";
                $answers .= (isset($_POST['bepaid']) && $_POST['bepaid'] == 1) ? "Y" : "N";
            } else {
                $questionsanswers .= "-";
                $answers .= "-";
            }

            //Paid More or Less than 4 years
            $questionsanswers .= "<br>Paid More or Less than 4 years: ";
            if ($_POST['recentjudge'] == Yes) {
                if ($_POST['bepaid'] == 1) {
                    $questionsanswers .= (isset($_POST['judgetime']) && $_POST['judgetime'] == 1) ? "Y" : "N";
                    $answers .= (isset($_POST['judgetime']) && $_POST['judgetime'] == 1) ? "Y" : "N";
                } else {
                    $questionsanswers .= "-";
                    $answers .= "-";
                }
            } else {
                $questionsanswers .= "-";
                $answers .= "-";
            }

            //Address more or less than 1 year
            $questionsanswers .= "<br>Address more or less than 1 year: ";
            $questionsanswers .= ($_POST['currentaddr'] == 1) ? "Y" : "N";
            $answers .= ($_POST['currentaddr'] == 1) ? "Y" : "N";

            //Driver's License
            $questionsanswers .= "<br>Driver's License: ";
            $questionsanswers .= ($_POST['audrivelic'] == 1) ? "Y" : "N";
            $answers .= ($_POST['audrivelic'] == 1) ? "Y" : "N";

            //Debt over 8K
            $questionsanswers .= "<br>Debt over 8K: ";
            $questionsanswers .= isUnsecuredDebtsOver8K() ? "Y" : "N";
            $answers .= isUnsecuredDebtsOver8K() ? "Y" : "N";

            // Get file with the rules for ALCForm
            $rules_alcform_csv = csv_to_array(ABSPATH . $_rules_csv);

            $rule = "";

            // Read each row from the file trying to match the rule with the answers
            if (is_array($rules_alcform_csv)) {
                foreach ($rules_alcform_csv as $row) {
                    $rule = trim($row['Citizen/Visa']) .
                    trim($row['DA/Bankrupt']) .
                    trim($row['Discharged More than 10 Years']) .
                    trim($row['Employed']) .
                    trim($row['More or Less than Year']) .
                    trim($row['Judgement/Default']) .
                    trim($row['Paid/Unpaid']) .
                    trim($row['Paid More or Less than 4 years']) .
                    trim($row['Address more or less than 1 year']) .
                    trim($row['Driver\'s License']) .
                    trim($row['Debt over 8K']);

                    // Get status defined for this rule
                    // NOTE: Specific RULE always overwrite Lender Status
                    if ($rule == $answers) {
                        if ((strlen(trim($row['Minimum Loan'])) > 0 && is_numeric($row['Minimum Loan'])) && (strlen(trim($row['Maximum Loan'])) > 0 && is_numeric($row['Maximum Loan']))) {
                            if ($loanAmount2 >= intval($row['Minimum Loan']) && $loanAmount2 <= intval($row['Maximum Loan'])) {
                                $LenderID = trim($row['Result']);
                                $rule_msg = $rule . ' | ' . trim($row['Minimum Loan']) . ' >= Loan <= ' . trim($row['Maximum Loan']);
                                break;
                            }
                        } else if (strlen(trim($row['Minimum Loan'])) > 0 && is_numeric($row['Minimum Loan'])) {
                            if ($loanAmount2 >= intval($row['Minimum Loan'])) {
                                $LenderID = trim($row['Result']);
                                $rule_msg = $rule . ' | Loan >= ' . trim($row['Minimum Loan']);
                                break;
                            }
                        } else if (strlen(trim($row['Maximum Loan'])) > 0 && is_numeric($row['Maximum Loan'])) {
                            if ($loanAmount2 <= intval($row['Maximum Loan'])) {
                                $LenderID = trim($row['Result']);
                                $rule_msg = $rule . ' | Loan <= ' . trim($row['Maximum Loan']);
                                break;
                            }
                        } /* else {
                    $LenderID = trim($row['Result']);
                    $rule_msg = $rule;
                    }*/
                    }
                }
            }
        } else {
            if (isUnsecuredDebts()) {

                //Debt over 8K
                $commetsauunseureddebts = isUnsecuredDebtsOver8K() ? "Yes" : "No";
                $comments2 .= '- Unsecure debts of $8,000 or more? - ' . $commetsauunseureddebts . ' <br> ';

                $questionsanswers .= "<br>Debt over 8K: ";
                $questionsanswers .= isUnsecuredDebtsOver8K() ? "Y" : "N";
                $answers .= isUnsecuredDebtsOver8K() ? "Y" : "N";

                //DA/Bankrupt
                $commetsHasbankrupt = ($_POST['bankrupt'] == 1) ? "Yes" : "No";
                $comments2 .= '- Bankrupt or Debt Agreement in past 10 years? - ' . $commetsHasbankrupt . ' ';

                $questionsanswers .= "<br>DA/Bankrupt: ";
                $questionsanswers .= ($_POST['bankrupt'] == 1) ? "Y" : "N";
                $answers .= ($_POST['bankrupt'] == 1) ? "Y" : "N";

            } else {
                $comments2 .= '- Australian citizen, permanent resident or visa? - <br> ';
                $comments2 .= '- Bankrupt or Debt Agreement in past 10 years? - <br> ';
                $comments2 .= '- Have you been discharged? - <br> ';
                $comments2 .= '- Currently employed? - <br> ';
                $comments2 .= '- Length at current job?(more than 1 year) -  <br> ';
                $comments2 .= '- Any judgments or defaults? - <br> ';
                $comments2 .= '- Paid or unpaid? - <br> ';
                $comments2 .= '- When did you pay your default/judgement?(more than 4 years) - <br> ';
                $comments2 .= '- Length at current address?(more than 1 year) - <br> ';
                $comments2 .= '- Australian driver licence - <br> ';
                $comments2 .= '- Unsecure debts of $8,000 or more? - ';
            }
        }

        if (isset($_ask_preferred_time_call_back) && $_ask_preferred_time_call_back && isset($_POST['prefCallBack'])) {
            $comments2 = '<br>Preferred time for call back: ' . $_POST['prefCallBack'] . '<br>________________________________________<br><br>' . getUnsecuredDebtsList() . (isPersonalQuestions() ? $comments2 : '');
        }

        //check spam border-top
        if (!isset($_POST['website_url']) || empty($_POST['website_url'])) {

            $data = array();
            $data['cid'] = strtolower($_cid);
            $data['title'] = isset($_POST['title']) ? $_POST['title'] : "";
            $data['firstName'] = isset($_POST['firstName']) ? $_POST['firstName'] : null;
            $data['lastName'] = isset($_POST['lastName']) ? $_POST['lastName'] : null;
            $data['loanAmount'] = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
            $data['typeOfLoan'] = $_POST['typeOfLoan'];
            $data['hasProperty'] = (isset($_POST['hasProperty']) && $_POST['hasProperty'] == 1) ? "true" : "false";
            $data['haveDeposit'] = (isset($_POST['haveDeposit']) && $_POST['haveDeposit'] == 1) ? "true" : "false";
            $data['realEstateValue'] = (isset($_POST['realEstateValue']) && !empty($_POST['realEstateValue'])) ? str_replace(array('$', ','), '', $_POST['realEstateValue']) : 0;
            $data['balanceOwing'] = (isset($_POST['balanceOwing']) && !empty($_POST['balanceOwing'])) ? str_replace(array('$', ','), '', $_POST['balanceOwing']) : 0;
            $data['mobileNumber'] = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $_POST['mobileNumber']);
            $data['landLineNumber'] = (isset($_POST['landLineNumber']) && !empty($_POST['landLineNumber'])) ? $_POST['landLineNumberAreaCode'] . $_POST['landLineNumber'] : '';
            $data['emailAddress'] = $_POST['emailAddress'];
            $data['suburb'] = isset($_POST['suburb']) ? $_POST['suburb'] : ' ';
            $data['state'] = $_POST['state'];
            $data['postCode'] = $_POST['postCode'];
            $data['referral'] = isset($_POST['referral']) ? $_POST['referral'] : isset($_SESSION['mm_adwords_ref']) ? $_SESSION['mm_adwords_ref'] : '';
            $data['comments'] = $comments2;
            $data['referrer'] = urlencode($_SESSION['mm_referalurl']);
            $data['t'] = $_SESSION['mm_adwords_t'];
            $data['k'] = $_SESSION['mm_adwords_k'];
            $data['a'] = $_SESSION['mm_adwords_a'];
            $data['platform'] = $platform;
            $data['UserHasDefaults'] = $_POST['recentjudge'];
            $data['UserLoanOver7k'] = isUnsecuredDebtsOver8K() ? "Yes" : "No";
            
            if(isset($_SESSION['mm_new_enquiry']) && $_SESSION['mm_new_enquiry']){
				$data['LenderID'] = '';
				$data['CallStatusID'] = '14';
			} else {
                $data['LenderID'] = $LenderID;
            }            

            //echo "<pre>"; print_r($data); echo "</pre>";
            // Setup cURL
            $data_string = json_encode($data);

            $ch = curl_init($_posturl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );

            $response = null;

            // Only for tests on ALCForm - user must be tester.alcform
            if (isset($_POST['firstName']) && $_POST['firstName'] == "tester.alcform") {
                die('<script type="text/javascript">alert("Lender ID: ' . $LenderID . ' Loan Type: ' . $_POST['typeOfLoan'] . '\n\nRule: ' . $rule_msg . '\n\nAnswers: ' . str_replace('<br>', '\n', $questionsanswers) . '");</script>');
            } else {
                // Send the request
                $response = curl_exec($ch);
            }

            date_default_timezone_set('Australia/Melbourne');

            // Check for errors
            if ($response === false) {

                // Record lead in log
                record_lead($_cid, $atts, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), "NO RESPONSE", curl_error($ch), $data_string));

                if (isset($_SESSION['failureURL']) && $_SESSION['failureURL'] != '') {
                    die('<script type="text/javascript">window.location.href="' . $_SESSION['failureURL'] . '";</script>');
                } else {
                    die(curl_error($ch));
                }
            }

            // Decode the response
            $responseData = json_decode($response, true);

            //echo "<pre>"; print_r($responseData); echo "</pre>";
            //echo "<pre>"; print_r($_POST); echo "</pre>";

            $responseString = "";

            if ($responseData['status'] == 'SUCCESS') {

                // Record lead in log
                record_lead($_cid, $atts, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), $responseData['status'], 'No', $data_string));

                $responseString = "<p class='api-post-success'>Successful</p>";

                if(isset($_SESSION['mm_new_enquiry']) && $_SESSION['mm_new_enquiry']) {
                    die('<script type="text/javascript">window.location.href="' . home_url() . '/complete/";</script>');
                }

                if (isUnsecuredDebts() && isUnsecuredDebtsOver8K() && isset($_POST['bankrupt']) && $_POST['bankrupt'] == 0) {

                    if ($_send_sms_customer) {
                        SendSMS($_sms_account_id, $_sms_user_email, $_sms_user_password, $data['mobileNumber'], str_replace('{{firstName}}', $data['firstName'], $_sms_message));
                    }

                    die('<script type="text/javascript">window.location.href="' . home_url() . '/complete/";</script>');
                }

                if ((isset($_sendemails) && $_sendemails == 'on') && $data['hasProperty'] == 'false' && $data['typeOfLoan'] !== '175' && $data['typeOfLoan'] !== '176' && $data['loanAmount'] > 49 && $data['loanAmount'] < 2501):
                    if ($data['firstName']) {
                        $ebroker_First_Name = $data['firstName'];
                    }

                    if ($data['mobileNumber']) {
                        $ebroker_Mobile = $data['mobileNumber'];
                    }

                    if ($data['loanAmount']) {
                        $ebroker_loanAmount = $data['loanAmount'];
                    }

                    if ($data['emailAddress']) {
                        $ebroker_Email = $data['emailAddress'];
                    }

                    if ($data['suburb']) {
                        $ebroker_Suburb = $data['suburb'];
                    }

                    if ($data['postCode']) {
                        $ebroker_Postcode = $data['postCode'];
                    }

                    $ebroker_URL = 'amount=' . $ebroker_loanAmount . '&partnerid=1&utm_source=ALC&utm_medium=ALC&utm_campaign=ALC&utm_term=ALC&First_Name=' . $ebroker_First_Name . '&Mobile=' . $ebroker_Mobile . '&email=' . $ebroker_Email . '&Postcode=' . $ebroker_Postcode . '';

                    die('<script type="text/javascript">window.location.href="' . home_url() . '/complete-personal/?_' . $ebroker_URL . '";</script>');
                endif;

                if ((isset($_sendemails) && $_sendemails == 'on') && $data['hasProperty'] == 'false' && ($data['typeOfLoan'] == '17' || $data['typeOfLoan'] == '2' || $data['typeOfLoan'] == '135')) {
                    if ($LenderID == 'DEC') {
                        die('<script type="text/javascript">window.location.href="' . home_url() . '/noproduct.php";</script>');
                    }

                    if ($LenderID == 'LNU') {
                        die('<script type="text/javascript">window.location.href="https://www.loanu.com.au/landing?user_id=15527";</script>');
                    }

                    if ($LenderID == 'PPL') {
                        die('<script type="text/javascript">window.location.href="https://apply.myloanportal.com/landingpage?p2=10812&utm_source=Pepper&utm_medium=Email&utm_campaign=Pepper";</script>');
                    }

                }

                if ($data['hasProperty'] == 'false' && $data['typeOfLoan'] != '176') {

                    if ($_send_sms_customer) {
                        SendSMS($_sms_account_id, $_sms_user_email, $_sms_user_password, $data['mobileNumber'], str_replace('{{firstName}}', $data['firstName'], $_sms_message));
                    }

                    die('<script type="text/javascript">window.location.href="' . home_url() . '/complete/";</script>');
                }

                if ((isset($_sendemails) && $_sendemails == 'on') && ($data['hasProperty'] == 'false' && $data['typeOfLoan'] == '176')):
                    $headers = array();
                    array_push($headers, 'From: ' . $company . ' <' . $_from_email . '>');
                    array_push($headers, 'Content-Type: text/html; charset=UTF-8');
                    if (isset($_partner_email) && !empty($_partner_email)):
                        $partner_email_html = '<h1>Applicant Details</h1>';
                        if ($data['loanAmount']) {
                            $partner_email_html .= '<p>Loan Amount: $' . $data['loanAmount'] . '</p><br>';
                        }

                        $partner_email_html .= '<p>Name: ';
                        if ($data['title']) {
                            $partner_email_html .= $data['title'] . ' ';
                        }

                        if ($data['firstName']) {
                            $partner_email_html .= $data['firstName'] . ' ';
                        }

                        if ($data['lastName']) {
                            $partner_email_html .= $data['lastName'] . ' ';
                        }

                        $partner_email_html .= '</p><br>';
                        if ($data['typeOfLoan'] == '175') {
                            $partner_email_html .= '<p>Business Type: New Business </p><br>';
                        }

                        if ($data['mobileNumber']) {
                            $partner_email_html .= '<p>Phone: ' . $data['mobileNumber'] . '</p><br>';
                        }

                        if ($data['emailAddress']) {
                            $partner_email_html .= '<p>Email: ' . $data['emailAddress'] . '</p><br>';
                        }

                        if ($data['suburb']) {
                            $partner_email_html .= '<p>Suburb: ' . $data['suburb'] . '</p><br>';
                        }

                        if ($data['state']) {
                            $partner_email_html .= '<p>State: ' . $data['state'] . '</p><br>';
                        }

                        if ($data['postCode']) {
                            $partner_email_html .= '<p>Post Code: ' . $data['postCode'] . '</p><br>';
                        }

                        if ($data['comments']) {
                            $partner_email_html .= '<p>Comments: ' . $data['comments'] . '</p><br>';
                        }

                        $sent = wp_mail($_partner_email, 'New Lead', $partner_email_html, $headers);
                        if (!$sent):
                            wp_mail('info@chillidee.com.au', 'Alert! Customer details not sent to partner', $partner_email_html, $headers);
                        endif;
                    endif;

                    $thanksEmail = file_get_contents(plugin_dir_path(__FILE__) . 'emails/' . $data['cid'] . '-thanks-email.html');
                    if ($thanksEmail && $data['emailAddress']):
                        wp_mail($data['emailAddress'], $company, $thanksEmail, $headers);
                    endif;

                    if (isset($_psurl) && $_psurl != ''):
                        die('<script type="text/javascript">window.location.href="' . $_psurl . '";</script>');
                    else:
                        if (isset($_SESSION['sucessURL']) && $_SESSION['sucessURL'] != '') {

                            if ($_send_sms_customer) {
                                SendSMS($_sms_account_id, $_sms_user_email, $_sms_user_password, $data['mobileNumber'], str_replace('{{firstName}}', $data['firstName'], $_sms_message));
                            }

                            die('<script type="text/javascript">window.location.href="' . home_url() . '/complete/";</script>');
                        }
                    endif;

                else:
                    {
                        if ($_send_sms_customer) {
                            SendSMS($_sms_account_id, $_sms_user_email, $_sms_user_password, $data['mobileNumber'], str_replace('{{firstName}}', $data['firstName'], $_sms_message));
                        }

                        die('<script type="text/javascript">window.location.href="' . home_url() . '/complete/";</script>');
                    }
                endif;
            } else {

                // Record lead in log
                record_lead($_cid, $atts, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), $responseData['status'], implode(", ", $responseData['errorMessage']), $data_string));

                $responseString = "<p class='api-post-failed'><ul>";
                foreach ($responseData['errorMessage'] as $value) {
                    $responseString .= "<li>" . htmlspecialchars($value) . "</li>";
                }
                $responseString .= "</ul></p>";
            }
        }
    }

    $jsonurl = $_geturl . $platform . '/' . strtolower($_cid);
    $json = file_get_contents($jsonurl);
    $json = json_decode($json, true);

    //echo "<pre>"; print_r($json); echo "</pre>";

    $_SESSION['sucessURL'] = $json['sucessURL'];
    $_SESSION['failureURL'] = $json['failureURL'];

    $typeOfLoan_options = '<option value="" selected="selected">Select loan type*</option>';
    if (isset($json['typeOfLoan'])) {
        foreach ($json['typeOfLoan'] as $key => $value) {
            $typeOfLoan_options .= '<option value="' . $key . '">' . $value . '</option>';
        }
    }

    $contact_form .= '<form id="apiEnquiryForm" name="apiEnquiryForm" action="" method="post">';

    //   $contact_form .= '<p style="color:#a0ce4e"> '. $mygooglekeyword .'</p>';

    if (!empty($responseString)) {
        $contact_form .= $responseString;
    }

    if (in_array('loanAmount', $json['fields'])) {
        $contact_form .= '<fieldset><p>
            <label for="loanAmount">Loan Amount ($)*</label>
            <input id="loanAmount" type="number" step="10" min="0" name="loanAmount" />
            <span class="alcerror" id="loanAmountError" style="color: red; display:none;"></span>
        </p>';
    }

    if (in_array('typeOfLoan', $json['fields'])) {
        $contact_form .= '<p>
        <label for="typeOfLoan">Type of Loan*</label>
        <select id="typeOfLoan" name="typeOfLoan">
          ' . $typeOfLoan_options . '
      </select>
      <span class="alcerror" id="typeOfLoanError" style="color: red; display:none;"></span>
  </p>';
    }

    if (in_array('hasProperty', $json['fields'])) {

        $contact_form .= '<p>
							<label>Do you currently own or paying off real estate?*</label>
							<input type="radio" id="hasPropertyYes" name="hasProperty" value="1" required></input>
							<label for="hasPropertyYes" class="radio">Yes</label>
							<input type="radio" id="hasPropertyNo" name="hasProperty" value="0" required></input>
							<label for="hasPropertyNo" class="radio">No</label>
							<span class="alcerror" id="hasPropertyError" style="color: red; display:none;"></span>
						</p>';

        $contact_form .= '<div id="yourPropertySection" class="yourPropertySection" style="display:none;">
							<p>Tell us a little about your property</p>
							<p>
								<label for="realEstateValue">Total real estate value ($)</label>
								<input id="realEstateValue" type="number" step="10" min="0" name="realEstateValue" />
								<span class="alcerror" id="realEstateValueError" style="color: red; display:none;"></span>
							</p>
							<p>
								<label for="balanceOwing">Balance Owing ($)</label>
								<input id="balanceOwing" type="number" step="1" min="0" name="balanceOwing" />
								<span class="alcerror" id="balanceOwingError" style="color: red; display:none;"></span>
							</p>
						</div>';

        $contact_form .= '<div id="bankruptSection" style="display:none;">
							<div class="label-block">
								<label for="referral">Have you been Bankrupt or in a Debt Agreement in the past 10 years?</label>
							</div>
							<div class="input-block">
								<p>
								<input type="radio" id="bankruptyes" name="bankrupt" value="1" required></input>
								<label for="bankruptyes" class="radio">Yes</label>
								<input type="radio" id="bankruptno" name="bankrupt" value="0" required></input>
								<label for="bankruptno" class="radio">No</label>
								</p>
							</div>
						</div>';
    }

    $contact_form .= '<input type="button" name="next0" class="next0 btn btn-info" value="Get Started" />';
    $contact_form .= '<div class="wrongmessagewrapper"><p id="wrongmessage" class="wrongmessage"></div>';
    $contact_form .= '</fieldset>';

    //New form --------------------------------------- begin
    $contact_form .= '<fieldset>
    <div class="label-block"><label for="referral">Are you an Australian citizen or permanent resident or do you hold a visa?</label></div>
            <div class="input-block">
            <input type="radio" id="hasAuvisayes" name="hasAuvisa" value="1" required></input>
            <label for="hasAuvisayes" class="radio">Yes</label>
            <input type="radio" id="hasAuvisano" name="hasAuvisa" value="0" required></input>
            <label for="hasAuvisano" class="radio">No</label>
    </div>

    <div class="label-block"><label for="referral">Have you been Bankrupt or in a Debt Agreement in the past 10 years?</label></div>
        <div class="input-block fieldset-bankrup">
            <input type="radio" id="bankruptyes" name="bankrupt" value="1" required></input>
            <label for="bankruptyes" class="radio">Yes</label>
            <input type="radio" id="bankruptno" name="bankrupt" value="0" required></input>
            <label for="bankruptno" class="radio">No</label>
        </div>

    <div id="discharged-block" class="discharged-block" style="display:none;">
        <div class="label-block"><label for="referral">Have you been discharged? <span class="tooltip1"><img src="https://www.australianlendingcentre.com.au/wp-content/uploads/2018/03/question-mark.png" class="tooltip-2" style="vertical-align: middle;margin-left: 7px;" width="20px" height="auto"> <span class="tooltiptext">Discharged is when you have been released from the bankruptcy or debt agreement status.</span></span></label> </div>
            <div class="input-block">
            <input type="radio" id="dischargedyes" name="discharged" value="1" required></input>
            <label for="dischargedyes" class="radio">Yes</label>
            <input type="radio" id="dischargedno" name="discharged" value="0" required></input>
            <label for="dischargedno" class="radio">No</label>
        </div>
    </div>

    <div class="label-block"><label>Are you currently employed?</label></div>
            <div class="input-block">
            <input type="radio" id="currentemplyes" name="currentempl" value="1" required></input>
            <label for="currentemplyes" class="radio">Yes</label>
            <input type="radio" id="currentemplno" name="currentempl" value="0" required></input>
            <label for="currentemplno" class="radio">No</label>
    </div>

    <div id="time-job-block" class="time-job-block" style="display:none;">
    <div class="label-block"><label>How long have you been in your current job?</label></div>
            <div class="input-block" style="margin-bottom: 20px;">
            <input type="radio" id="currentjob2yrmore" name="currentjob2yr" value="1" required></input>
            <label for="currentjob2yrmore" class="radio">More than 1 year</label>
            <input type="radio" id="currentjob2yrless" name="currentjob2yr" value="0" required></input>
            <label for="currentjob2yrless" class="radio">Less than 1 year</label>
            </div>
    </div>
    ';

    $contact_form .= '<input type="button" name="next2" class="next2 btn btn-info" value="Next" style="margin-top: 20px;"/>';
    $contact_form .= '<input type="button" name="previous" class="previous btn btn-default" value="Previous" />';
    $contact_form .= '<div class="wrongmessagewrapper"><p id="wrongmessage2" class="wrongmessage"></div>';
    $contact_form .= '</fieldset>';

    $contact_form .= '<fieldset>
            <div class="label-block"><label for="referral">Do you have any recent judgments or defaults?</label></div>
                <div class="input-block">
                <input type="radio" id="recentjudgeyes" name="recentjudge" value="Yes" required></input>
                <label for="recentjudgeyes" class="radio">Yes</label>
                <input type="radio" id="recentjudgeno" name="recentjudge" value="No" required></input>
                <label for="recentjudgeno" class="radio">No</label>
                </div>

            <div id="paid-block" style="display:none;">
                <div class="label-block"><label for="referral">Are they paid or unpaid?</label></div>
                <div class="input-block">
                <input type="radio" id="paidyes" name="bepaid" value="1" required></input>
                <label for="paidyes" class="radio">Paid</label>
                <input type="radio" id="paidno" name="bepaid" value="0" required></input>
                <label for="paidno" class="radio">Unpaid</label>
                </div>
            </div>


            <div id="judgetime-block" style="display:none;">
                <div class="label-block"><label for="referral">How long has it been since you paid your default/judgement?</label></div>
                <div class="input-block">
                <input type="radio" id="judgetime2yrmore" name="judgetime" value="1" required></input>
                <label for="judgetime2yrmore" class="radio">More than 4 years</label>
                <input type="radio" id="judgetime2yrless" name="judgetime" value="0" required></input>
                <label for="judgetime2yrless" class="radio">Less than 4 years</label>
                </div>
            </div>

                <div class="label-block"><label for="referral">How long have you been living at your current address for?</label></div>
                <div class="input-block">
                <input type="radio" id="currentaddr2yrmore" name="currentaddr" value="1" required></input>
                <label for="currentaddr2yrmore" class="radio">More than 1 year</label>
                <input type="radio" id="currentaddr2yrless" name="currentaddr" value="0" required></input>
                <label for="currentaddr2yrless" class="radio">Less than 1 year</label>
                </div>


                <div class="label-block "><label for="referral">Do you have an Australian driver licence?</label></div>
                <div class="input-block">
                <input type="radio" id="audrivelicyes" name="audrivelic" value="1" required></input>
                <label for="audrivelicyes" class="radio">Yes</label>
                <input type="radio" id="audrivelicno" name="audrivelic" value="0" required></input>
                <label for="audrivelicno" class="radio">No</label>
                </div>
    ';

    $contact_form .= '<input type="button" name="next3" class="next3 btn btn-info" value="Next" style="margin-top: 20px;"/>';
    $contact_form .= '<input type="button" name="previous" class="previous btn btn-default" value="Previous" />';
    $contact_form .= '<div class="wrongmessagewrapper"><p id="wrongmessage3" class="wrongmessage"></div>';
    $contact_form .= '</fieldset>';

    //New form --------------------------------------- end

    //Unsecured Debt form --------------------------------------- end

    $contact_form .= '<fieldset>
                        <p><strong>To save time, please enter all your debts:</strong></p>
                        <div id="unsecuredDebtFields">
                            <p>
                                <label for="typeOfUnsecuredDebt">Type*</label>
                                <select id="typeOfUnsecuredDebt" name="typeOfUnsecuredDebt[]">
                                    <option value="" selected="selected">Select type*</option>
                                    <option value="I have no debts" class="noUnsecuredDebt">I have no debts</option>
                                    <option value="Car Loans">Car Loans</option>
                                    <option value="Credit Card or Store Card">Credit Card or Store Card</option>
                                    <option value="Defaulted Debts">Defaulted Debts</option>
                                    <option value="Disconnected Phone Bills">Disconnected Phone Bills</option>
                                    <option value="Disconnected Utilities">Disconnected Utilities</option>
                                    <option value="Legal Bills">Legal Bills</option>
                                    <option value="Medical Bills">Medical Bills</option>
                                    <option value="Money Owed to Friends or Family">Money Owed to Friends or Family</option>
                                    <option value="Overdrafts">Overdrafts</option>
                                    <option value="Pay Day Loans or Short Term Finance">Pay Day Loans or Short Term Finance</option>
                                    <option value="Personal Loan">Personal Loan</option>
                                    <option value="Tax Debts">Tax Debts</option>
                                </select>
                                <span class="alcerror" id="typeOfUnsecuredDebtError" style="color: red; display:none;">Type is required</span>
                                <input type="hidden" id="unsecuredDebt_Type" name="unsecuredDebt_Type[]"/>
                            </p>
                            <p id="unsecuredDebtCreditLimitSection" class="unsecuredDebtField" style="display:none">
                                <label for="unsecuredDebt_CreditLimit">Credit Limit ($)*</label>
                                <input id="unsecuredDebt_CreditLimit" type="number" step="10" min="0" name="unsecuredDebt_CreditLimit[]">
                                <span class="alcerror" id="unsecuredDebt_CreditLimitError" style="color: red; display:none;">Credit
                                    Limit is required and must be greater than 0</span>
                            </p>
                            <p id="unsecuredDebtAmountSection" class="unsecuredDebtField" >
                                <label for="unsecuredDebt_Amount">Amount Owing ($)*</label>
                                <input id="unsecuredDebt_Amount" type="number" step="1" min="0" name="unsecuredDebt_Amount[]">
                                <span class="alcerror" id="unsecuredDebt_AmountError" style="color: red; display:none;">Amount Owing is
                                    required and must be greater than 0</span>
                            </p>
                            <p id="unsecuredDebtProviderOwedToSection" class="unsecuredDebtField">
                                <label for="unsecuredDebt_ProviderOwedTo">Provider/Owed To*</label>
                                <input id="unsecuredDebt_ProviderOwedTo" type="text" name="unsecuredDebt_ProviderOwedTo[]">
                                <span class="alcerror" id="unsecuredDebt_ProviderOwedToError" style="color: red; display:none;">Provider/Owed
                                    To is required</span>
                            </p>
                        </div>
                        <input type="button" id="addUnsecuredDebt" class="unsecuredDebtField" name="addUnsecuredDebt" class="btn btn-info" value="Add New/Save">
                        <br />
                        <p id="allUnsecuredDebtLabel" class="unsecuredDebtField"><strong>ALL UNSECURED DEBT</strong></p>
                        <div id="unsecuredDebtList" class="unsecuredDebtField"></div>
                        <div class="unsecuredDebtTotalLabel unsecuredDebtField"><p><strong>Total: </strong></p></div>
                        <div class="unsecuredDebtTotal unsecuredDebtField"><input type="text" id="unsecuredDebtTotal" name="unsecuredDebtTotal" value="0" readonly></div>';

    $contact_form .= '<input type="button" name="next4" class="next4 btn btn-info" value="Next" style="margin-top: 20px;"/>';
    $contact_form .= '<input type="button" name="previous4" class="previous4 btn btn-default" value="Previous" />';
    $contact_form .= '<div class="wrongmessagewrapper"><p id="wrongmessage4" class="wrongmessage"></div>';
    $contact_form .= '</fieldset>';

    //Unsecured Debt form --------------------------------------- end

    if (in_array('title', $json['fields'])) {
        $contact_form .= '<p>
    <label>Title</label>
    <input type="radio" id="titleMr" name="title" value="Mr"></input>
    <label for="titleMr" class="radio">Mr</label>
    <input type="radio" id="titleMrs" name="title" value="Mrs"></input>
    <label for="titleMrs" class="radio">Mrs</label>
    <input type="radio" id="titleMs" name="title" value="Ms"></input>
    <label for="titleMs" class="radio">Ms</label>
    <input type="radio" id="titleDr" name="title" value="Dr"></input>
    <label for="titleDr" class="radio">Dr</label>
    <span class="alcerror" id="titleError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('firstName', $json['fields'])) {
        $contact_form .= '<fieldset><p>
    <label for="firstName">First Name*</label>
    <input id="firstName" type="text" name="firstName" />
    <span class="alcerror" id="firstNameError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('lastName', $json['fields'])) {
        $contact_form .= '<p>
    <label for="lastName">Last Name*</label>
    <input id="lastName" type="text" name="lastName" />
    <span class="alcerror" id="lastNameError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('mobileNumber', $json['fields'])) {
        $contact_form .= '<p>
    <label for="mobileNumber">Mobile number*</label>
    <input id="mobileNumber" type="number" min="0" name="mobileNumber" />
    <span class="alcerror" id="mobileNumberError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('landLineNumber', $json['fields'])) {
        $contact_form .= '<p>
    <label for="landLineNumber">Landline number</label>
    <select name="landLineNumberAreaCode" id="landLineNumberAreaCode" class="landLineNumberAreaCode">
        <option selected="selected" value="02">02</option>
        <option value="03">03</option>
        <option value="07">07</option>
        <option value="08">08</option>
    </select>
    <input id="landLineNumber" type="number" min="0" name="landLineNumber" class="landLineNumber" />
    <span class="alcerror" id="landLineNumberError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('emailAddress', $json['fields'])) {
        $contact_form .= '<p>
    <label for="emailAddress">Email*</label>
    <input id="emailAddress" type="email" name="emailAddress" />
    <span class="alcerror" id="emailAddressError" style="color: red; display:none;"></span>
</p>';
    }

    if (isset($_ask_preferred_time_call_back) && $_ask_preferred_time_call_back) {
        $contact_form .= '<p>
        <label for="prefCallBack">Preferred time for call back*</label>
        <select id="prefCallBack" name="prefCallBack">
          <option value="Anytime" selected="selected">Anytime</option>
		  <option value="Morning">Morning</option>
		  <option value="Afternoon">Afternoon</option>
		  <option value="Evening">Evening</option>
		  <option value="Weekend">Weekend</option>
      </select>
      <span class="alcerror" id="prefCallBackError" style="color: red; display:none;"></span>
		</p>';
    }

    $contact_form .= '<input type="button" name="next1" class="next1 btn btn-info" value="Next" />';
    $contact_form .= '<input type="button" name="previous1" class="previous1 btn btn-default" value="Previous" />';
    $contact_form .= '<div class="wrongmessagewrapper"><p id="wrongmessage1" class="wrongmessage"></div>';
    $contact_form .= '</fieldset>';

    if (in_array('suburb', $json['fields'])) {
        $contact_form .= '<p>
    <label for="suburb">Suburb*</label>
    <input id="suburb" type="text" name="suburb" />
    <span class="alcerror" id="suburbError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('state', $json['fields'])) {
        $contact_form .= '<fieldset><div style="width: 35%;display: inline-block; margin-top:0; float: left;">
    <label for="state">State*</label>
    <select id="state" name="state" class="state">
        <option value="ACT">ACT</option>
        <option selected="selected" value="NSW">NSW</option>
        <option value="NT">NT</option>
        <option value="QLD">QLD</option>
        <option value="SA">SA</option>
        <option value="TAS">TAS</option>
        <option value="VIC">VIC</option>
        <option value="WA">WA</option>
    </select>
</div>';
    }

    if (in_array('postCode', $json['fields'])) {
        $contact_form .= '<div style="width: 60%;display: inline-block;margin-left: 5%; margin-top:0;">
    <label for="postCode">Post Code*</label>
    <input id="postCode" type="text" name="postCode" class="postCode" />
    <span class="alcerror" id="postCodeError" style="color: red; display:none;"></span>
</div>';
    }

    if (in_array('referral', $json['fields'])) {
        $contact_form .= '<p>
    <label for="referral">Where did you hear about us?*</label>
    <select name="referral" id="referral" class="referral">
        <option selected="selected" value="">[Select One]</option>
        <option value="Yahoo">Yahoo</option>
        <option value="Radio">Radio</option>
        <option value="Newspaper">Newspaper</option>
        <option value="Google">Google</option>
        <option value="Television">Television</option>
        <option value="Family/Friend">Family/Friend</option>
        <option value="Email">Email</option>
        <option value="Other/Not Sure">Other/Not Sure</option>
    </select>
    <span class="alcerror" id="referralError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('comments', $json['fields'])) {
        $contact_form .= '<p>
    <label for="comments">Comments</label>
    <textarea id="comments" name="comments"></textarea>
</p>';
    }

    $contact_form .= '<input type="text" name="website_url" id="website_url" />';

    $contact_form .= '<div id="error_message"></div>';

    $contact_form .= '<p>* indicates required information.</p>';

    $contact_form .= '<div class="squaredFour">';
    $contact_form .= '<input type="checkbox" checked id="terms" /><label for="terms"></label>';
    $contact_form .= '<p>' . $_privacy_text . ' <a href="' . $_privacy . '">' . $_privacy_link_text . '</a></p>';
    $contact_form .= '<span class="alcerror" id="termsError" style="color: red; display:none;margin-top:20px;"></span>';
    $contact_form .= '</div>';

    $contact_form .= '<input type="hidden" name="alcapiformsumbit" value="1" id="alcapiformsumbit">';
    $contact_form .= '<input type="button" name="apiSubmit" value="Submit" id="apiSubmit">';
    $contact_form .= '<input type="button" name="previous" class="previous btn btn-default" value="Previous" /></fieldset>';

    $contact_form .= '</form>';

    $contact_form .= '<div class="nocreaitimapct"><img class="alignnone size-full wp-image-17829" src="/wp-content/uploads/2017/09/no-effect-on-credit-check.png" alt="NO-effect-on-your-credit-file" width="18" height="21" style="margin-right: 10px;">NO effect on your credit file</div>';

    return $contact_form;
}

function register_alcapiform_shortcodes()
{
    add_shortcode('alcapi_contact_form', 'alcapiform_shortcode_api_contact_form');
}

add_action('init', 'register_alcapiform_shortcodes');
//sample usage: [iframe_contact_form baseurl="http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx" style="width: 90%; height: 900px;" cid="CC" svs="0" css="http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css"]
//create setting page
if (is_admin()) {
    include "settings.php";
    $alcapiform_settings_page = new ALCAPIFormSettingsPage();
}

//create setting quick link
function alcapiform_plugin_action_links($links, $file)
{
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=alcapiform-setting-admin">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}

add_filter('plugin_action_links', 'alcapiform_plugin_action_links', 10, 2);

//functions
if (!function_exists("TimStartsWith")) {

    function TimStartsWith($haystack, $needle)
    {
        return !strncmp($haystack, $needle, strlen($needle));
    }

}

function csv_to_array($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename)) {
        return false;
    }

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false) {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
            if (!$header) {
                $header = $row;
            } else {
                $data[] = array_combine($header, $row);
            }

        }
        fclose($handle);
    }
    return $data;
}

function isUnsecuredDebts()
{
    $loanAmount_temp = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
    return $_POST['hasProperty'] == '0' && $loanAmount_temp <= 2500 && ($_POST['typeOfLoan'] == '126' || $_POST['typeOfLoan'] == '17' || $_POST['typeOfLoan'] == '135');
}

function isPersonalQuestions()
{
    $loanAmount_temp = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
    return $_POST['hasProperty'] == '0' && $loanAmount_temp > 2500 && $loanAmount_temp <= 50000 && ($_POST['typeOfLoan'] == '2' || $_POST['typeOfLoan'] == '17' || $_POST['typeOfLoan'] == '135');
}

function record_lead($app, $atts, $lead)
{

    try {
        $options = get_option('alcapiform_option_name');

        $_record_leads = isset($atts['record_leads']) ? $atts['record_leads'] : $options['record_leads'];
        $_send_email_lead_fail = isset($atts['send_email_lead_fail']) ? $atts['send_email_lead_fail'] : $options['send_email_lead_fail'];
        $_email_lead_fail = isset($atts['email_lead_fail']) ? $atts['email_lead_fail'] : $options['email_lead_fail'];
        $_ignore_customers_lead_fail = isset($atts['ignore_customers_lead_fail']) ? $atts['ignore_customers_lead_fail'] : $options['ignore_customers_lead_fail'];

        $company = get_bloginfo('name');
        $header = array('First Name', 'Loan Amount', 'Type Of Loan', 'Mobile Number', 'Email Address', 'Platform', 'Date Time', 'Response', 'Error Message', 'Full Request');
        $status = $lead[7];
        $customer = $lead[0];
        $developer_email = "boni@chillidee.com.au";

        if (isset($_ignore_customers_lead_fail)) {

            $ignore_customers_lead_fail = explode(', ', $_ignore_customers_lead_fail);

            if (in_array($customer, $ignore_customers_lead_fail)) { //Ignore SPAM from customer
                return;
            }
        }

        $filepath = WP_CONTENT_DIR . '/leads/' . $app . '_' . date("Y") . '_' . date("m") . '_leads_log.csv';

        if (!file_exists(WP_CONTENT_DIR . '/leads')) {
            mkdir(WP_CONTENT_DIR . '/leads', 0777, true);
        }

        $file_exists = file_exists($filepath);
        $file = fopen($filepath, 'a');

        if ($file === false) {
            wp_mail($developer_email, $company . " - Record Lead failed", 'Failed to open temporary file ' . $filepath);
        } else {

            if (!$file_exists) {
                // save the column headers
                fputcsv($file, $header);
            }

            $loan_desc = '';

            switch ($lead[2]) {
                case 126:
                    $loan_desc = "Debt Consolidation";
                    break;
                case 18:
                    $loan_desc = "Refinance";
                    break;
                case 17:
                    $loan_desc = "Personal";
                    break;
                case 2:
                    $loan_desc = "Motor Vehicle";
                    break;
                case 135:
                    $loan_desc = "Other";
                    break;
                case 175:
                    $loan_desc = "Loan for New Business";
                    break;
                case 176:
                    $loan_desc = "Loan for Existing Business";
                    break;
                case 166:
                    $loan_desc = "Short Term Loans";
                    break;
                case 165:
                    $loan_desc = "Self Employed Loans";
                    break;
                case 164:
                    $loan_desc = "Private Funding";
                    break;
            }

            $lead[2] = $loan_desc;

            // Record lead in CSV file (log)
            if ($_record_leads) {
                fputcsv($file, $lead);
            }

            fclose($file);

            // Send email if lead failed
            if ($_send_email_lead_fail && $status != 'SUCCESS') {

                array_pop($header);
                array_pop($lead);

                $html = '<p>Please contact <b>' . $company . '</b> customer <b>' . $customer . '</b> who is having problems submitting his/her lead on <a href="' . home_url() . '">' . home_url() . '</a></p>';
                $html .= '<br/>Details below:<br/><br/>';
                $html .= '<table>
							<tr>
								<td width="150">' . implode(": <br/>", $header) . '</td>
								<td width="600">' . implode("<br/>", $lead) . '</td>
							</tr>
						  </table>';

                wp_mail(array($developer_email, $_email_lead_fail), $company . " - Lead failed", $html);
            }
        }
        //}
    } catch (Exception $e) {}
}

// Esendex
// https://developers.esendex.com/api-reference

function FilterSMS()
{

    $loanAmount = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
    $hasProperty = (isset($_POST['hasProperty']) && $_POST['hasProperty'] == 1) ? true : false;

    // Loan amount under $6000 and without property - No SMS to be sent
    if ($loanAmount < 6000 && $hasProperty == false) {
        return true;
    }

    return false;
}

function SendSMS($AccountID, $Email, $Password, $Recipient, $Message)
{
    try {

        if (FilterSMS()) { // Additional filters that determine whether or not the SMS will be sent
            return;
        }

        date_default_timezone_set('Australia/Melbourne');

        $timestamp = new DateTime();
        $smsdate = clone $timestamp;

        // Working hours
        // ==============================
        $options = get_option('alcapiform_option_name');

        $_opening_hours = $options['opening_hours'];
        $_closing_hours = $options['closing_hours'];
        $_closing_hours_friday = $options['closing_hours_friday'];
        $_lunch_starts = $options['lunch_starts'];
        $_lunch_ends = $options['lunch_ends'];

        $opening_hours = new DateTime();
        $opening_hours->setTime(date('H', strtotime($_opening_hours)), date('i', strtotime($_opening_hours)), 00);

        $closing_hours = new DateTime();
        $closing_hours->setTime(date('H', strtotime($_closing_hours)), date('i', strtotime($_closing_hours)), 00);

        $closing_hours_friday = new DateTime();
        $closing_hours_friday->setTime(date('H', strtotime($_closing_hours_friday)), date('i', strtotime($_closing_hours_friday)), 00);

        $lunch_starts = new DateTime();
        $lunch_starts->setTime(date('H', strtotime($_lunch_starts)), date('i', strtotime($_lunch_starts)), 00);

        $lunch_ends = new DateTime();
        $lunch_ends->setTime(date('H', strtotime($_lunch_ends)), date('i', strtotime($_lunch_ends)), 00);
        // ==============================

        $dt1 = strtotime($timestamp->format(DateTime::ISO8601));
        $dt2 = date("l", $dt1);
        $dt3 = strtolower($dt2);

        if ($dt3 == "saturday" || $dt3 == "sunday" || ($dt3 == "friday" && $timestamp > $closing_hours_friday)) {

            // When Saturdays or Sundays at anytime or Fridays after 5:00 PM -> Schedule SMS at 8:30 AM on Monday
            $smsdate->modify('next monday');
            $smsdate->setTime(date('H', strtotime($_opening_hours)), date('i', strtotime($_opening_hours)), 00);

        } else {

            // When Mondays - Thursdays after 5:30 PM -> Schedule SMS at 8:30 AM on the Following Day
            if ($dt3 != "friday" && $timestamp > $closing_hours) {

                $smsdate->modify('+1 days');
                $smsdate->setTime(date('H', strtotime($_opening_hours)), date('i', strtotime($_opening_hours)), 00);

            } elseif ($timestamp < $opening_hours) {

                // When Mondays - Fridays before 8:30 AM -> Schedule SMS at 8:30 AM on the Same Day
                $smsdate->setTime(date('H', strtotime($_opening_hours)), date('i', strtotime($_opening_hours)), 00);

            } elseif ($timestamp > $lunch_starts && $timestamp < $lunch_ends) {

                // When Lunch time -> Schedule SMS at 1:30 PM on the Same Day
                $smsdate->setTime(date('H', strtotime($_lunch_ends)), date('i', strtotime($_lunch_ends)), 00);

            } else {
                // Otherwise, it sends it right away
                $smsdate = clone $timestamp;
            }
        }

        if ($_POST['firstName'] == "tester.sms") {
            echo '<p>The SMS would be sent to the mobile number:<br/><strong>' . $Recipient . '</strong> on <strong>' . $smsdate->format('d/m/Y') . '</strong> at <strong>' . $smsdate->format('h:i:s A') . '</strong></p>';
            echo '<br/><p>Message:</p><br/><p><strong>' . nl2br($Message) . '</strong></p>';
            die();
        }

        $data = null;

        if ($timestamp != $smsdate) { // Schedule
            $data = array('accountreference' => $AccountID,
                'sendat' => $smsdate->format(DateTime::ISO8601), /* yyyy-MM-ddThh:mm:ssZ */
                'messages' => array(0 => array('to' => $Recipient, 'body' => $Message)));
        } else { // Right away
            $data = array('accountreference' => $AccountID,
                'messages' => array(0 => array('to' => $Recipient, 'body' => $Message)));
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.esendex.com/v1.0/messagedispatcher");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Authorization: Basic " . base64_encode($Email . ":" . $Password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        //die('<script type="text/javascript">alert("'.var_dump($result).');</script>');

    } catch (Exception $e) {
        //die('<script type="text/javascript">alert("Error: ' . $e->getMessage() . '");</script>');
    }
}

function isUnsecuredDebtsOver8K()
{
    $unsecuredDebtTotal_temp = isset($_POST['unsecuredDebtTotal']) ? str_replace(array('$', ','), '', $_POST['unsecuredDebtTotal']) : 0;
    return $unsecuredDebtTotal_temp > 8000;
}

function getUnsecuredDebtsList()
{

    $unsecuredDebtsList = '';

    if (isset($_POST['typeOfUnsecuredDebt']) && count($_POST['typeOfUnsecuredDebt']) > 1) {

        $unsecuredDebtsList .= 'All Unsecured Debt:<br><br>';

        for ($i = 1; $i < count($_POST['typeOfUnsecuredDebt']); $i++) {

            $unsecuredDebtsList .= 'Num. ' . $i . '<br>';

            $unsecuredDebtsList .= 'Type: ' . $_POST['unsecuredDebt_Type'][$i] . '<br>';
            if ($_POST['unsecuredDebt_Type'][$i] == 'Credit Card or Store Card') {
                $unsecuredDebtsList .= 'Credit Limit: $' . number_format($_POST['unsecuredDebt_CreditLimit'][$i]) . '<br>';
            }
            $unsecuredDebtsList .= 'Amount Owing: $' . number_format($_POST['unsecuredDebt_Amount'][$i]) . '<br>';
            $unsecuredDebtsList .= 'Provider/Owed To: ' . $_POST['unsecuredDebt_ProviderOwedTo'][$i] . '<br><br>';
        }

        $unsecuredDebtsList .= 'Total: ' . $_POST['unsecuredDebtTotal'];

        $unsecuredDebtsList .= '<br>________________________________________<br><br>';

    }

    return $unsecuredDebtsList;
}
